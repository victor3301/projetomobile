package com.example.ProjetoEscola;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ProjetoEscola.dao.AlunosBd;
import com.example.ProjetoEscola.model.Alunos;

public class Formulario extends AppCompatActivity {
    EditText editText_nome, editText_cidade, editText_cpf;
    Button btn_poli;
    Alunos editarAluno, aluno;
    AlunosBd dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        aluno = new Alunos();
        dao = new AlunosBd(Formulario.this);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cadastro de Estudantes");

        Intent intent = getIntent();
        editarAluno = (Alunos) intent.getSerializableExtra("aluno-escolhido");


        editText_nome = (EditText) findViewById(R.id.editText_nome);
        editText_cidade = (EditText) findViewById(R.id.editText_cidade);
        editText_cpf = (EditText) findViewById(R.id.editText_cpf);
        btn_poli = (Button) findViewById(R.id.btn_poli);

        //
        if (editarAluno != null){
            btn_poli.setText("Editar Aluno");

            editText_nome.setText(editarAluno.getNomeAluno());
            editText_cidade.setText(editarAluno.getCidade());
            editText_cpf.setText(editarAluno.getCpf()+"");
            aluno.setId(editarAluno.getId());

        }else{
            btn_poli.setText("Cadastrar Novo Aluno");
        }
        btn_poli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aluno.setNomeAluno(editText_nome.getText().toString());
                aluno.setCidade(editText_cidade.getText().toString());
                aluno.setCpf(Integer.parseInt(editText_cpf.getText().toString()));

                Intent intent = new Intent(Formulario.this, MainActivity.class);
                startActivity(intent);

                if(btn_poli.getText().toString().equals("Cadastrar Novo Aluno")){
                    dao.salvarAluno(aluno);
                    dao.close();
                }else{
                    dao.alterarAluno(aluno);
                    dao.close();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
