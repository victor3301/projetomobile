package com.example.ProjetoEscola.model;

import java.io.Serializable;

public class Alunos implements Serializable {
    private Long id;
    private String nomeAluno;
    private String cidade;
    private int cpf;

    @Override
    public String toString() {
        return nomeAluno.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }
}
