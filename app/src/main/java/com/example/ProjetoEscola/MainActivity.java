package com.example.ProjetoEscola;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ProjetoEscola.dao.AlunosBd;
import com.example.ProjetoEscola.model.Alunos;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lista;
    AlunosBd dao;
    ArrayList<Alunos> listview_Alunos;
    Alunos aluno;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.listview_Alunos);
        registerForContextMenu(lista);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {

                Alunos alunoEscolhido = (Alunos) adapter.getItemAtPosition(position);

                Intent i = new Intent(MainActivity.this, Formulario.class);
                i.putExtra("aluno-escolhido", alunoEscolhido);
                startActivity(i);
            }
        });
        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long id) {
                aluno = (Alunos) adapter.getItemAtPosition(position);
                return false;
            }
        });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Escola");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.android_menu_operacoes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menuItemCadastrar:
                Intent intent = new Intent(this, Formulario.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    protected void onResume(){
        super.onResume();
        carregarAluno();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem menuDelete = menu.add("Deletar Este Aluno");
        menuDelete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                dao = new AlunosBd(MainActivity.this);
                dao.deletarAluno(aluno);
                dao.close();
                carregarAluno();
                return true;
            }
        });
    }

    public void carregarAluno(){
        dao = new AlunosBd(MainActivity.this);
        listview_Alunos = dao.getLista();
        dao.close();

        if (listview_Alunos !=null){
            adapter = new ArrayAdapter<Alunos>(MainActivity.this, android.R.layout.simple_list_item_1, listview_Alunos);
            lista.setAdapter(adapter);
        }

    }
}
