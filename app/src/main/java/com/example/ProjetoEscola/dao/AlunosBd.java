package com.example.ProjetoEscola.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ProjetoEscola.model.Alunos;

import java.util.ArrayList;


public class AlunosBd extends SQLiteOpenHelper {

    private static final String DATABASE = "bdAlunos";
    private static final int VERSION = 1;

    public AlunosBd (Context context){
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String aluno = "CREATE TABLE alunos(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nomealuno TEXT NOT NULL, cidade TEXT NOT NULL, cpf INTEGER);";
        db.execSQL(aluno);
    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String aluno = "DROP TABLE IF EXISTS alunos";
        db.execSQL(aluno);
    }

    //metodo de salvar
    public void salvarAluno(Alunos aluno){
        ContentValues values = new ContentValues();
        values.put("nomealuno",aluno.getNomeAluno());
        values.put("cidade",aluno.getCidade());
        values.put("cpf",aluno.getCpf());

        getWritableDatabase().insert("alunos",null,values);
    }

    //metodo alterar
    public void alterarAluno(Alunos aluno){
        ContentValues values = new ContentValues();
        values.put("nomealuno",aluno.getNomeAluno());
        values.put("cidade",aluno.getCidade());
        values.put("cpf",aluno.getCpf());

        String [] args = {aluno.getId().toString()};

        getWritableDatabase().update("alunos",values,"id=?", args);
    }

    //metodo deletar
    public void deletarAluno(Alunos aluno){
        String [] args = {aluno.getId().toString()};

        getWritableDatabase().delete("alunos","id=?", args);
    }

    //metodo de listar
    public ArrayList<Alunos> getLista(){
        String [] columns = {"id","nomealuno","cidade","cpf"};
        Cursor cursor = getWritableDatabase().query("alunos",columns,null,null,null,null,null,null);
        ArrayList<Alunos> alunos = new ArrayList<Alunos>();

        while (cursor.moveToNext()){
            Alunos aluno = new Alunos();
            aluno.setId(cursor.getLong(0));
            aluno.setNomeAluno(cursor.getString(1));
            aluno.setCidade(cursor.getString(2));
            aluno.setCpf(cursor.getInt(3));
            alunos.add(aluno);

        }
        return alunos;
    }
}
